import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('icsanspromoxhypervisors')


def test_ntp_enabled_and_started(host):
    service = host.service("ntp")
    assert service.is_running
    assert service.is_enabled
